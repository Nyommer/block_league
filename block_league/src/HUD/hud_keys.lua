function block_league.HUD_keys_create(p_name)
  Panel:new("bl_keys", {
    player = p_name,
    bg = "bl_hud_keyboard.png",
    bg_scale = { x = 4, y = 4 }
  })
end



function block_league.HUD_keys_remove(arena)
  for pl_name, _ in pairs(arena.players) do
    panel_lib.get_panel(pl_name, "bl_keys"):remove()
  end
end